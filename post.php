<?php
  require('config/config.php');

  $err = "";
  
  // Check if id is pass in URL
  if (!isset($_GET['id'])) {
    header('Location: index.php');
    exit;
  }

  $posts = get_content("posts");
  $post = $posts[$_GET['id']];

  if (empty($post)) {
    header('Location: index.php');
    exit;
  }

  if (!empty ($_POST)) {
    if (isset($_POST['submit']) && empty($_POST['content']) ){
      $err = "Write Something";
      header('Location: post.php?id=' .$_GET['id']);
      exit;
    }

    $date = date_create();
    $created_at = date_timestamp_get($date);
    $updated_at = date_timestamp_get($date);
  
    $comments = new Comments();
    $comments->setContent($_POST['content']);
    $comments->setAuthor($_SESSION["user"]);
    $comments->setPostId($_GET["id"]);
    $comments->setCreatedAt($created_at);
    $comments->setUpdatedAt($updated_at);

    add_content($comments, "comments");

    header('Location: post.php?id=' .$_GET['id']);
    exit;
  }
  
  $allComments = get_content('comments');
  $comments = [];
  foreach ($allComments as $key => $comment) {
    if ($comment["post_id"] == $_GET["id"]) {
      array_push($comments, $comment);
    }
  }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <?php include('includes/head.php') ?>
  <title>Article | Uniblog</title>
  <style>
    figure {
      position: relative;
      height: 250px;
      width: 100%;
      overflow: hidden;
    }
    img {
      position: absolute;
      top: 50%;
      width: 100%;
      transform: translateY(-50%);
    }
    figure:after {
      position: absolute;
      content: '';
      z-index: 1;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, .3);
    }
    .img {
      position: relative;
    }
    .back {
      position: absolute;
      z-index: 3;
      top: 20px;
      left: 45px;   
    }
    .post-title {
      position: absolute;
      z-index: 3;
      bottom: 10px;
      left: 45px
    }
    .card-body {
      min-height: 100vh;
    }
    ::-webkit-scrollbar {
      width: 10px;
    }
    ::-webkit-scrollbar-track {
      border-radius: 5px;
      box-shadow: inset 0 0 10px rgba(14, 180, 28, 0.25);
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 5px;
      background-color: #18CD5B;
    }
    ::-webkit-scrollbar-thumb:hover {
      background-color: #5FFC99;
    }
  </style>
</head>
<body>
  <?php include('includes/header.php') ?>
  <div class="card-body">
    <div class="img">
      <div class="back">
        <a class="btn btn-primary" href="index.php">Back to home</a>
        <a class="btn btn-warning" href="editPost.php?id=<?= $_GET['id'] ?>">Update post</a>
        <a class="btn btn-danger" href="core/deletePost.php?id=<?= $_GET['id'] ?>">Delete post</a>
      </div>
      <figure class="mb-0">
        <img src="<?= $post['img'] ?>" alt="<?= $post['title'] ?>">
      </figure>
      <h1 class="post-title text-white"><?= $post['title'] ?></h1>
    </div>
    <article class="px-4 py-3 mx-4 shadow rounded-bottom">
      <p class="text-muted">By <a href="#"><?= $post['author'] ?></a> | <?= date('d M Y H:i', $post['created_at']) ?></p>
      <p><?= $post['content'] ?></p>
      <h2>Commentary</h2>
      <?php if (!empty($_SESSION["user"])) { ?>
        <form class="row g-3 needs-validation mb-2" novalidate method="POST">
          <?php if(!empty($err)) { ?>
            <div class="alert alert-secondary" role="alert">
              <p><?= $err ?></p>
            </div>
          <?php } ?>
          <div class="col-md-4">
            <label for="validationCustom01" class="form-label">Enter commentary</label>
            <textarea class="form-control" id="content" name="content" rows="3" required></textarea>
            <br/>
          </div>
          <div class="col-12">
            <button class="btn btn-primary" type="submit" id="submit" value="submit" name="submit">Submit</button>
          </div>
        </form>
      <?php } ?>
      <?php foreach (array_reverse($comments) as $key => $comment){ ?>
        <div class="alert alert-secondary" role="alert">
          <p class="mb-0"><?= $comment["content"] ?></p>
          <small>Posted by <a class="text-capitalize" href="#"><?= $comment["author"]?></a> - <?= date('d M Y H:i', $post['created_at']) ?></small>
        </div>
      <?php } ?>
    </article>
  </div>
  <?php include('includes/footer.php') ?>
</body>
</html>
